# Cartoonify Backend API

## API Documentation for using cURL commands on postman

### https://documenter.getpostman.com/view/13060005/UVJcmwnz

## Please replace the URL https://cartoonifyapp.herokuapp.com/api/cartoon/create/ to http://127.0.0.1:8000/api/cartoon/create/ in your development environment to make requests via postman.

## Installation Steps for unix interface

- Upgrade pip and create python3 environment and activate the new environment

```(bash)
python3 -m pip install --upgrade pip
python3 -m pip install --user --upgrade pip
python3 -m pip install --user virtualenv
python3 -m venv env
source env/bin/activate
```

- Install requirements.txt which contains all the dependencies for this app

```(bash)
python3 -m pip install -r requirement.txt
```

- cd to api directory

```(bash)
python3 manage.py makemigrations
python3 manage.py migrate
```

- run the following command to run test

```(bash)
python3 manage.py test
```

- run the following command to run the server

```(bash)
python3 manage.py runserver
```
=======
