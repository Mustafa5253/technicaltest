from io import BytesIO
from django.core.files import File
from PIL import Image
#import cv2
#import requests
from PIL import ImageFilter


def make_cartoon(image):

    im = Image.open(image)


    #Applying Image filters
    im = im.filter(ImageFilter.BoxBlur(3))
    im = im.convert(mode="P",palette=Image.ADAPTIVE,colors=32)

    #img = img.convert(mode="RGB")

    if(im.mode in ("RGBA", "P")):
        im=im.convert('RGB') # convert mode

    #im.thumbnail(size) # resize image

    thumb_io = BytesIO() # create a BytesIO object

    im.save(thumb_io, 'JPEG', quality=85) # save image to BytesIO object

    cartooned_picture = File(thumb_io, name=image.name) # create a django friendly File object


    return cartooned_picture