from django.db import models
from cartoonify.core_feature import make_cartoon

# Create your models here.

def upload_path(instance, filename):
    return 'originals/{filename}'.format(filename=filename)

def thumb_upload(instance, filename):
    return 'cartooned/cartooned_{filename}'.format(filename=filename)

class Upload(models.Model):
    Image_name = models.CharField(max_length=32, blank=False)
    original_image = models.ImageField(blank=True, null=True, upload_to=upload_path)
    cartooned_image = models.ImageField(blank=True, null=True, upload_to=thumb_upload)

    def save(self, *args, **kwargs):
        self.cartooned_image = make_cartoon(self.original_image)

        super().save(*args, **kwargs)