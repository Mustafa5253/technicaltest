from rest_framework import serializers
from cartoonify.models import Upload
from django.conf import settings


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Upload
        fields = ('id','Image_name','original_image','cartooned_image')