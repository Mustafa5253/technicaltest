from rest_framework.test import APITestCase, APIClient
from django.urls import reverse
import os
import io
from PIL import Image

class TestSetup(APITestCase):

    def generate_photo_file(self):
        file = io.BytesIO()
        image = Image.open("test_image.jpg")
        image.save(file, 'png')
        file.name = 'test.png'
        file.seek(0)
        return file

    def setUp(self):
        self.upload_image_url = reverse("cartoonify:upload_image")
        self.view_data_url = reverse("cartoonify:viewdata")

        image_file = self.generate_photo_file()
        self.user_data = {'Image_name':"Test_Image" , "original_image":image_file}
        #print(self.user_data)

        return super().setUp()

    def tearDown(self):
        return super().tearDown()