from .test_setup import TestSetup
from django.urls import reverse

class TestViews(TestSetup):

    def test_user_cannot_upload_image_with_no_data(self):
        res=self.client.post(self.upload_image_url)

        self.assertEqual(res.status_code , 400)

    
    def test_user_uploads_image_with_data(self):
        res=self.client.post(self.upload_image_url,self.user_data, format="multipart")
        #import pdb
        #pdb.set_trace()
        self.assertEqual(res.status_code , 200)

    def test_user_can_download_images_after_uploading(self):
        response=self.client.post(self.upload_image_url,self.user_data, format="multipart")
        #import pdb
        #pdb.set_trace()
        id = int(response.data["id"])
        #print("ID: ",id)
        self.download_image_url = reverse("cartoonify:download" , args=(id,))
        res=self.client.get(self.download_image_url)
        self.assertEqual(res.status_code , 200)

    def test_user_can_view_available_data(self):
        response = self.client.get(self.view_data_url)

        self.assertEqual(response.status_code , 200)
