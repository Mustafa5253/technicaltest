from django.contrib import admin
from django.urls import path
from cartoonify.views import UploadImage , ViewList , FileDownloadAPI

app_name = 'cartoonify'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('cartoon/create/', UploadImage.as_view(), name='upload_image'),
    path('cartoon/view/', ViewList.as_view(), name='viewdata'),
    path('cartoon/download/<int:id>/',FileDownloadAPI.as_view(), name="download"),
]
