from django.shortcuts import get_object_or_404
from cartoonify.models import Upload
from cartoonify.serializers import PostSerializer
from rest_framework import viewsets, filters, generics, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
import json

class UploadImage(APIView):

    parser_classes = [MultiPartParser, FormParser]

    def post(self, request, format=None):
        try:
            print(request.data)
            serializer = PostSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as err:

            errors = {"erros":{"message" : str(err)}}
            return Response(errors, status=500)





class ViewList(generics.ListAPIView):

    serializer_class = PostSerializer
    queryset = Upload.objects.all()


class FileDownloadAPI(generics.ListAPIView):


    def get(self, request, id, format=None):
        try:
            queryset = Upload.objects.get(id=id)
            serializer_class = PostSerializer(queryset)
            #print("Serial : ",serializer_class)
            original = serializer_class.data["original_image"]
            cartooned = serializer_class.data["cartooned_image"]
            data = {'original_image' :original , 'cartooned_image':cartooned}

            return Response(data, status=status.HTTP_200_OK)
        except Exception as err:

            errors = { "erros": { "message" : str(err)}}
            return Response(errors, status=500)

